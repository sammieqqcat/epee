# A developement of epee library [2017-07-02]
<!-- TOC -->
## Outline  
- [Overview](#overview)
- [How to build](#how-to-build)
    - [Environment](#environment)
- [Examples](#examples)
- [Roadmap](#roadmap)
- [Change Log](#change-log)

<!-- /TOC -->
## Overview  
This project serves to help someone to learn basic APIs of the [epee](https://github.com/sabelnikov/epee) library.  

## How to build  
### Environment  
+ g++ (GCC) 6.3.1 20161221 (Red Hat 6.3.1-1)  
+ boost libraries 1.60  

TBA...  


## Examples  
TBA...  
There are some refactored examples (not documented yet) under the `example` folder.  

## Roadmap  
+ WIP: Refactor all the platform-dependent interfaces to the platform-independent ones

## Change Log  
### [2017-07-09]  
+ **Added**   
    - document partially the `misc_log_ex.h` file  

### [2017-07-08]  
+ **Added**  
    - re-structure the `README.md` file as  
        * Overview  
        * How to build  
        * Examples  
        * Change Log  
    - an example for `epee::console_handlers_binder` class , detailed as `examples/console-handler/consoleHandlerBinderDemo.cpp`  
+ **Update**  
    - rename folder `sammy` to `examples`, which consists of examples demonstrating the usage of apis of `epee` library

### [2017-07-04]  
+ **Added**  
    - document the `include/net/http_base.h`  
    - add a `Doxyfile` to configure documentation of the library  

### [2017-07-02]  
+ **Added**  
    - a server demo serving rpc request in json, detailed as `rpcJsonSrvDemo.cpp`  
    - a client utility serving to interact with the server above, detailed as `rpcJsonClientDemo.cpp`  
    - a client demo serving to interact with the server in json but not according to rpc, detailed as `jsonClientDemo.cpp`  
    - a client demo serving to interacti with the server in binary, detailed as `binClientDemo.cpp`   
    - add a rpc request in json with error respons, but its usage remains to figure out     
