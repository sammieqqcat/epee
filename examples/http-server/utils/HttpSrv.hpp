/**
*@file  HttpSrv.hpp
*@brief     tool class implementing the fucntion as a server
*@author    sammietocat
*@version   2017-07-02
*/
#pragma once

#include <iostream>
#include <map>

#include "commons.hpp"

#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include "net/http_server_cp2.h"
#include "net/http_server_handlers_map2.h"

#include "Cmd.hpp"

using namespace epee;
using namespace std;

namespace sammy {
// a type defintion for convience
typedef epee::net_utils::connection_context_base connCtx;

/**
*@brief     server object
*@note  child of {@link net_utils::http::i_http_server_handler} in
*"http_protocol_handler.h"
*/
class HttpSrv : public net_utils::http::i_http_server_handler<
                    epee::net_utils::connection_context_base> {
  public:
    HttpSrv();
    ~HttpSrv();

    bool bind2Address( const string &ip, const string &port );
    bool start();
    bool isRunning();
    bool timedWait( const int seconds = 100000 );
    bool finalize();
    /* interface for console handling */
    bool send_stop_signal();
    /* interface for console handling */
  private:
    CHAIN_HTTP_TO_MAP2( connCtx ); // forward http requests to uri map

    BEGIN_URI_MAP2()
    MAP_URI_AUTO_BIN2( "/onBinHello", onSayHello, sCmd )
    MAP_URI_AUTO_JON2( "/onJsonHello", onSayHello, sCmd )
    BEGIN_JSON_RPC_MAP( "/sammy_rpc" )
    MAP_JON_RPC( "sayHello", onSayHello, sCmd )
    MAP_JON_RPC_WE( "cannotSayHello", onCannotSayHello,
                    sCmd ) // this is still unclear
    END_JSON_RPC_MAP()
    END_URI_MAP2()

    bool onSayHello( const sCmd::request &req, sCmd::response &resp,
                     connCtx &ctx );
    bool onCannotSayHello( const sCmd::request &req, sCmd::response &resp,
                           epee::json_rpc::error &errResp, connCtx &ctx );

    // defined in "include/net/http_server_cp2.h"
    net_utils::boosted_http_server_custum_handling hdl;
    std::atomic<bool>                              running;

    // a dummy database consisting of (key,value) for testing
    map<string, string> db;
};
};