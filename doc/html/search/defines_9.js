var searchData=
[
  ['kv_5fchain_5fbase',['KV_CHAIN_BASE',['../keyvalue__serialization_8h.html#a342a14c7fc21f07676570139be7352a4',1,'keyvalue_serialization.h']]],
  ['kv_5fchain_5fmap',['KV_CHAIN_MAP',['../keyvalue__serialization_8h.html#a4441673b79758271a35d474b6c28748c',1,'keyvalue_serialization.h']]],
  ['kv_5fserialize',['KV_SERIALIZE',['../keyvalue__serialization_8h.html#a1ae3e24f4adf87459a57908cbcae50ea',1,'keyvalue_serialization.h']]],
  ['kv_5fserialize_5fcontainer_5fpod_5fas_5fblob',['KV_SERIALIZE_CONTAINER_POD_AS_BLOB',['../keyvalue__serialization_8h.html#a5198d282e1173312c5606fcd4af5e458',1,'keyvalue_serialization.h']]],
  ['kv_5fserialize_5fcontainer_5fpod_5fas_5fblob_5fn',['KV_SERIALIZE_CONTAINER_POD_AS_BLOB_N',['../keyvalue__serialization_8h.html#a1bb8a28181227352e712b7cbffbf2f89',1,'keyvalue_serialization.h']]],
  ['kv_5fserialize_5fn',['KV_SERIALIZE_N',['../keyvalue__serialization_8h.html#ae927b4f417c09b67783089d9b81a5a3e',1,'keyvalue_serialization.h']]],
  ['kv_5fserialize_5fval_5fpod_5fas_5fblob',['KV_SERIALIZE_VAL_POD_AS_BLOB',['../keyvalue__serialization_8h.html#a0f124f8be4067e3783262d9a343907f1',1,'keyvalue_serialization.h']]],
  ['kv_5fserialize_5fval_5fpod_5fas_5fblob_5fforce',['KV_SERIALIZE_VAL_POD_AS_BLOB_FORCE',['../keyvalue__serialization_8h.html#a2b805ecdb595f39056986217bb0f3cdf',1,'keyvalue_serialization.h']]],
  ['kv_5fserialize_5fval_5fpod_5fas_5fblob_5fforce_5fn',['KV_SERIALIZE_VAL_POD_AS_BLOB_FORCE_N',['../keyvalue__serialization_8h.html#a8197c3a912b120cfb48d27b2124caba7',1,'keyvalue_serialization.h']]],
  ['kv_5fserialize_5fval_5fpod_5fas_5fblob_5fn',['KV_SERIALIZE_VAL_POD_AS_BLOB_N',['../keyvalue__serialization_8h.html#aa498b6fb6ff13410435ea87aa1ca161b',1,'keyvalue_serialization.h']]]
];
